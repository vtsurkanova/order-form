//import 'bootstrap/dist/css/bootstrap.min.css';
import '@fortawesome/fontawesome-free/js/all.min';
import './scss/style.scss';


// регистрация события загрузки документа.
if (window.addEventListener) window.addEventListener("load", init, false);
else if (window.attachEvent) window.attachEvent("onload", init);

// регистрация обработчиков событий элементов формы.
function init() {
    form1.fname.onchange = nameOnChange;
    form1.email.onchange = emailOnChange;
    form1.password.onchange = passwordOnChange;
    form1.cardNumber.onchange = cardNumberOnChange;
    form1.cvv.onchange = cvvOnChange;
    form1.onsubmit = onsubmitHandler;
}

function validate(elem, pattern) {
    var res = elem.value.search(pattern);
    if (res == -1) elem.className = "invalid";
    else elem.className = "valid";
}

function nameOnChange() {
    var pattern = /\S/;
    validate(this, pattern);
}

function emailOnChange() {
    var pattern = /\b[a-z0-9._]+@[a-z0-9.-]+\.[a-z]{2,4}$/;
    validate(this, pattern);
}

function passwordOnChange() {
    var pattern = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$/;
    validate(this, pattern);
}

function cvvOnChange() {
    var pattern = /\d{3}$/;
    validate(this, pattern);
}

function cardNumberOnChange() {
    var pattern = /\d{16}$/;
    validate(this, pattern);
}

function onsubmitHandler() {

    var invalid = false;

    for (var i = 0; i < form1.elements.length; ++i) {
        var e = form1.elements[i];
        // проверка типа элемента и наличия обработчика события onchange.
        if (e.type == "text" && e.onchange) {
            e.onchange(); // запуск события onchanhe
            if (e.className == "invalid") invalid = true;
        }
    }

    if (invalid) {
        // alert("Допущены ошибки при заполнении формы.");
        return false; // отмена отправки формы.
    }
}
