let webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
//const HtmlWebpackPlugin = require('html-webpack-plugin');
const autoprefixer = require('autoprefixer');
let conf = {
	entry: './src/index.js',
	output: {
		path: path.resolve(__dirname,'./dist'),
		//path: __dirname + '/dist',
		filename: 'main.js',
		publicPath: 'dist/'
	},
	devServer: {
		overlay: true
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				loader: 'babel-loader',
				//exclude: '/node_modules/'
			},
			{
		        test: /\.scss$/,
		        use: ExtractTextPlugin.extract(
		          {
		            //fallback: 'style-loader',
		            use: ['css-loader',
		            	{
			              loader: 'postcss-loader',
			              options: {
			                plugins: () => [autoprefixer()]
			              }
			            },
			            'sass-loader'],
		          })
		    },
			{
				test: /\.css$/,
				use: ExtractTextPlugin.extract({
					//fallback: "style-loader",
					use: ['css-loader']
				})
			}
		]
	},
	plugins: [
		new ExtractTextPlugin(
	      {filename: 'style.css'}
	    ),
	]
};

module.exports = conf;